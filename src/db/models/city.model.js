import mongoose from 'mongoose';
// create a schema
const City = new mongoose.Schema({
   name: String,
   country: String,
   population: Number,
   salt: String,

});
export default mongoose.model('City', City);