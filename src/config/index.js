import dotenv from 'dotenv'

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const envFound = dotenv.config({path:'.env.develop'});

if (!envFound){
    throw new Error('Couldnt find .env file');

}

export default{
    ENV: process.env.NODE_ENV,
    PORT: process.env.PORT,
    DEV_ID:process.env.DEV_NAME,
    MONGODB_URI:process.env.MONGODB_URI,
    JWT_SECRET:process.env.JWT_SECRET
}
