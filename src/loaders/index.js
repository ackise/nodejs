import expressLoader from './express';
import {connectToMongoDb} from './mongodb'


export default async ({expressApp})=>{

    await connectToMongoDb();
    console.log('Mongo is connected')

    await expressLoader({app:expressApp});
    console.log('Express Loader')



}