import mongoose from 'mongoose'
import config from '../config'

export async function connectToMongoDb(){
    const connection = await mongoose
        .connect( config.MONGODB_URI, {
            useNewUrlParser:true,
            useCreateIndex:true,
            useUnifiedTopology:true
        });
    return connection.connection.db;
}