import {CityModel} from '../db/models/'




export default class CityService {
    static async  createCity ({name,country,population}){
        return CityModel.create({
            name,
            country,
            population
        })

    }
}