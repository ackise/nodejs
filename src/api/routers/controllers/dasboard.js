import {Router} from 'express';
import config from '../../../config';

const route = Router();
export default app => {
	app.use('/dashboard', route);
	/**
	 * @desc Роут має бути лише для авторизованих користувачів, тобто потрібно додати мідлвере для перевірки токену
	 **/
	route.get('/list', (request, response) => {
		response.json({
			success: true,
			data: []
		});
	});
}