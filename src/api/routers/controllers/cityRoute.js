import {Router} from 'express';
import CityService from '../../../services/CityService';
import {celebrate, Joi, errors, Segments} from 'celebrate'

const route = Router();
export default function (app) {
	app.use('/city', route);
    route.post('/', 
    celebrate({
        body: Joi.object({
            name:Joi
            .string()
            .required()
            ,
            country:Joi.string().required(),
            population:Joi.number().required()
        })
    }),
    
    async (request, response) => {
        const {name,country,population}=request.body
		const city = await CityService.createCity({
            name,
            country,
            population
        });
        response.json({
            success:true,
            data: city
        })
	});
    
    
}