import { Router } from 'express';
import authRoute from './routers/controllers/authRoute';
import homeRoute from './routers/controllers/homeRoute'
import productsRoute from './routers/controllers/productsRoute';
import sendGreetingRoute from './routers/controllers/sendGreetingRoute';
import cityRoute from './routers/controllers/cityRoute'

export default () => {
	const app = Router();
	homeRoute(app)
	productsRoute(app)
	sendGreetingRoute(app)
	authRoute(app)
	cityRoute(app)

	return app;
}